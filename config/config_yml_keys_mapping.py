#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guozg
@File：config_yml_keys_mapping.py
"""


class ConfYmlKeys:
    """"""

    def __init__(self):
        """"""
        # -------------- 环境配置 需要用的一些key --------------
        # 需要用到的一些key
        self.host_key = "host"
        self.url_key = "url"
        self.unify_host_key = "unify_host"
        self.web_login_path_key = "web_login_path"
        self.app_login_path_key = "app_login_path"
        self.web_request_data_key = "web_request_data"
        self.app_request_data_key = "app_request_data"
        self.case_login_name_key = "case_login_name"
        # 下面的key 为data里的key
        self.data_key = "data"
        self.type_key = "type"
        self.loginType_key = "loginType"
        self.username_key = "username"
        self.password_key = "password"
        self.code_key = "code"
        self.loginId_key = "loginId"
        self.device_key = "device"
        # ------------------------ yml文件常用到的一些变量值的字符串 ---------------
        # api_str 对应的key
        self.api_str_key = "api_str"
        # local_str 对应的key
        self.local_str_key = "local_str"
        # global_str 对应的key
        self.global_str_key = "global_str"
        # app_str 对应的key
        self.app_str_key = "app_str"
        # web_str 对应的key
        self.web_str_key = "web_str"
        # request_str 对应的key
        self.request_str_key = "request_str"
        # response_str 对应的key
        self.response_str_key = "response_str"
        # login_str 对应的key
        self.login_str_key = "login_str"
        # yml_str 对应的key
        self.yml_str_key = "yml_str"
        # json_str 对应的key
        self.json_str_key = "json_str"
        # sql_str 对应的key
        self.sql_str_key = "sql_str"
        # all_str 对应的key
        self.all_str_key = "all_str"
        # second_str 对应的key
        self.second_str_key = "second_str"
        # minute_str 对应的key
        self.minute_str_key = "minute_str"
        # hour_str 对应的key
        self.hour_str_key = "hour_str"
        # day_str 对应的key
        self.day_str_key = "day_str"

        # ---------------登录方法名称----------
        # 每个class下全局登录时，登录的方法名称
        self.test_init_key = "test_init"

        # ------- case和page yml文件 进行操作的一些配置  ----------
        self.yml_dir_key = "yml_dir_name"
        # 处理后的yml文件保存的文件目录
        self.handled_yml_dir_key = "handled_yml_dir"
        # 斜杠
        self.slash_key = "slash"
        # 反斜杠
        self.backslash_key = "backslash"

        #-------------- 保存依赖的case的结果 用到的一些key的值 ------
        self.caseid_key = "caseid"
        self.filetype_key = "filetype"
        self.filedata_key = "filedata"
        self.testinit_key = "testinit"

        # ---------- case_id case层与page层之间的连接的标志等 --------
        self.case_join_page_key = "case_join_page"
        self.yml_join_func_key = "yml_join_func"
        self.key_join_key = "key_join"

        # ----------接口运行结果 (json()) 保存的目录及其他相关的配置-------
        self.jsonfile_dir_key = "jsonfile_dir_name"
        self.unify_savevalue_key = "unify_save_value"
        self.save_type = "save_type"

        # ------------ session 限制 -------------------
        self.websession_limit_key = "web_session_limit"
        self.appsession_limit_key = "app_session_limit"
        self.android_session_limit_key = "android_session_limit"
        self.ios_session_limit_key = "ios_session_limit"
