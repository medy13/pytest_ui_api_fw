#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guo
@File：read_yml_file.py
"""

import yaml


class ReadYml:
    """读取yml文件,并返回yml文件的内容"""

    def get_yml_data(self, ymlfile_path: str) -> dict:
        """获取yml文件的数据"""
        data = yaml.safe_load(open(ymlfile_path, "r", encoding="utf-8"))
        return data

    def get_yml_func_data(self, ymlfile_path: str, func_name: str) -> dict:
        """获取yml文件中 case或方法 所对应的数据"""
        ymldata = self.get_yml_data(ymlfile_path)
        data = ymldata[func_name]
        return data
