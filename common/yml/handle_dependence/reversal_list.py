#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guo
@File：reversal_list.py
"""

class RevList:

    def rev_list(self, lst: list) -> list:
        """反转list,暂时无用"""
        lst = lst
        tmp = []

        # 先判断是否为list
        if isinstance(lst, list):
            # 当为list时，先对list的值进行反转
            lst = lst[::-1]
            # 判断是否为空list
            if len(lst) < 1:
                return None

            for item in lst:
                # 判断lst 的值的类型是否为list
                if isinstance(item, list):
                    value = self.rev_list(item)
                    # 判断是否为None
                    if value != None:
                        tmp.append(self.rev_list(value))
                else:
                    tmp.append(item)

        # else:
        #     tmp.append(lst)

        return tmp