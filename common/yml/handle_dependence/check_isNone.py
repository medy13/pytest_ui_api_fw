#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guo
@File：check_isNone.py
"""


class CheckNone:

    def check_isNone(self, checkdata) -> None or str:
        """
        检测传入的参数是否为None,为None则返回None;否则返回传入的值  \n
        主要是为了防止同事在yml文件中将None值写错. \n
        :param checkdata: 要校验的值
        :return: None or str
        """
        data = checkdata
        # 先校验是否为None
        if data == None:
            data = None
        elif str.lower(data) == "none" or data == "null" or data == "~":
            data = None
        return data
