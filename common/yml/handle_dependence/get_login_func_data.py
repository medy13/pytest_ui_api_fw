#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guo
@File：get_login_func_data.py
"""

class GetLoginData:
    """获取登录的yml参数值"""

    def get_login_func_data(self, case_id: str, login_func_name: str) -> dict:
        """获取登录的yml参数值"""
        caseid = case_id
        login_func = login_func_name
        lst = caseid.split(self.case_join_page_flag)[0].split(self.yml_join_func_flag)
        # 获取当前的yml文件名称
        yml_file = lst[0]
        # 获取当前case/被依赖的case的 对应的yml文件的绝对路径
        # yml_file_path = self.__fpath.get_file_abspath_yml(self.yml_dir, yml_file)
        yml_file_path = self.__fpath.get_file_abspath_yml(yml_file)
        # 获取当前case/被依赖的case的 对应的登录的 请求参数值(如loginId,password)
        logindata = self.__ryml.get_yml_func_data(yml_file_path, login_func)
        return logindata
