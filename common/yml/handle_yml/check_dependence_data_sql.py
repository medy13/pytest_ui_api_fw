#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Create Time: 2021/1/15 上午9:55
@Author: guo
@File：check_dependence_data_sql.py
"""
import pytest

from common.yml.get_yml_keys import GetYmlKeys


class CheckSql:
    """"""

    def checkdata(self, page_apidata: dict, ymlkeys: GetYmlKeys) -> str:
        """
        当dependence_type为sql时,校验sqlstatement的值是否为空.当为空,或 非str 或非 list时  \n
        直接将case置为fail. 目前没有实现sql的操作的逻辑.  \n
        :param page_apidata:
        :param ymlkeys:
        :return:
        """

        api_data = page_apidata
        yml_keys = ymlkeys

        key = yml_keys.get_sqlstatement_key()
        dep_case_data_key = yml_keys.get_dependence_case_data_key()
        key_value = api_data[dep_case_data_key][key]

        if key_value == None:
            pytest.fail(f"当前的数据依赖的类型为: sql, 但是 {key}的值为 None.故将case置为fail")
        elif not isinstance(key_value, str) and not isinstance(key_value, list):
            pytest.fail(f"当前的数据依赖的类型为: sql, 但是 {key}的类型,即不是 str ,也不是 list."
                        f"故将当前case置为fail.")
        return key_value
