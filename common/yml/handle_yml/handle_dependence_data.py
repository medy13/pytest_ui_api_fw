#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Create Time: 2021/1/14 下午3:46
@Author: guo
@File：handle_dependence_data.py
"""
import sys
import pytest

sys.path.append("../../")
from common.yml.get_yml_keys import GetYmlKeys
from config.get_conf_data import GetConfData


class HandleDependenceData:
    """"""
    def __init__(self):
        self.__conf = GetConfData()

    def get_dependence_data(self,page_api_data:dict,yml_keys:GetYmlKeys,case_api_data:dict=None,req_keys:list=None)->dict:
        """
        处理数据依赖相关的数据.默认值为false,不依赖.  \n
        :param page_api_data: 对应的page层的yml里的 api_data 的数据
        :param yml_keys: yml的keys
        :param case_api_data: 传入的case层 yml数据 对应的 api_data的数据
        :param req_keys: 传入的case层的yml数据 对应的key
        :return: dict
        """
        api_data = page_api_data
        yml_keys = yml_keys
        req_params = case_api_data
        req_keys = req_keys

        flag = False
        dep_case_key = yml_keys.get_dependence_case_key()
        dep_case_data_key = yml_keys.get_dependence_case_data_key()
        key_value = None
        # 先判断 case层的yml数据是否为None
        if req_keys !=None:
            if dep_case_key in req_keys:
                key_value = req_params[dep_case_key]

        if key_value == True:
            flag = True

        #进行赋值
        api_data[dep_case_key] = flag

        # 开始进行判断
        if flag == False:
            api_data[dep_case_data_key] = None

        # 表示有数据依赖
        else:
            # 进入到这到一步,说明传入了case层的yml
            """
            第一步,先获取case层的数据,然后通过for循环的方式,赋值给page层.这样就不会出现 报 key error的情况.
            即case层未写相关的key,也不会报错.因为page层有相关的key
            需要注意的是:在这次for循环替换数据时,不能进行替换case_id.而case_id的值,将放在第三步进行.
            """
            # 先判断case层是否存在dependence_case_data. 若存在,由通过for循环的方式进行赋值
            if dep_case_data_key in req_keys:
                req_dep_case_data:dict = req_params[dep_case_data_key]
                caseid_key = yml_keys.get_dependence_case_id_key()
                for key in req_dep_case_data.keys():
                    if key != caseid_key:
                        api_data[dep_case_data_key][key] = req_dep_case_data[key]

                """
                第二步,处理dependence_type的值,尤其是当未填写时.dependence_type目前有四种值,分别为:json,request,response以及sql
                目前也只处理了json,request,response 这三种情况.当为sql时的逻辑还没实现,暂不考虑,后续再实现.
                默认值为json.
                """
                type_key = yml_keys.get_dependence_type_key()
                request_str = self.__conf.get_request_str()
                response_str = self.__conf.get_response_str()
                json_str = self.__conf.get_json_str()
                sql_str = self.__conf.get_sql_str()

                type_value = api_data[dep_case_data_key][type_key]
                type = json_str
                # 进行判断相应的值.
                if type_value == None or not isinstance(type_value,str):
                    type = type
                else:
                    type_value = str.lower(type_value)
                    if sql_str in type_value:
                        type = sql_str

                    elif request_str in type_value:
                        type = request_str

                    elif response_str in type_value:
                        type = response_str

                    else:
                        type = json_str

                # 进行赋值
                api_data[dep_case_data_key][type_key] = type

                """
                第三步,此时要校验type为 json,sql,request,response 时,对应的jsonfilepath,sqlstatement,case_id是否都有值
                并且这些值是否符合校验的规则.当不符合或没有值时,直接将case置为fail
                在此步进行校验的过程中,当type为 request,response时,是需要校验 page层的case_id 和 case层的case_id的.
                校验通过后,需要对case_id的值进行拼接的.
                """
                # dependence_type 为 request 或response
                if type == response_str or type == request_str:
                    from common.yml.handle_yml.check_dependence_data_yml import CheckYml
                    cyml = CheckYml()
                    caseid = cyml.check_data(api_data,req_params,yml_keys)
                    api_data[dep_case_data_key][caseid_key] = caseid
                
                # dependence_type 为: json
                elif type == json_str:
                    """"""
                    from common.yml.handle_yml.check_dependence_data_json import CheckJson
                    cjson = CheckJson()
                    jsonfilepath_key = yml_keys.get_jsonfilepath_key()
                    jsonfilepath_value = cjson.checkdata(api_data,yml_keys)
                    api_data[dep_case_data_key][jsonfilepath_key] = jsonfilepath_value

                # dependence_type 为: sql
                else: 
                    """"""
                    from common.yml.handle_yml.check_dependence_data_sql import CheckSql
                    csql = CheckSql()
                    sqlstatement_key = yml_keys.get_sqlstatement_key()
                    sqlstatement_value = csql.checkdata(api_data,yml_keys)
                    api_data[dep_case_data_key][sqlstatement_key] = sqlstatement_value


            # 表示case层未写,此处不用写任何的逻辑代码,或者直接此处将case置为fail
            else:
                pytest.fail(f"当前case存在着数据依赖,但是传入的case层的yml文件里,没有写dependence_case_data,所以将该case置为fail,\n"
                            f"请认真检测case层 和 page层的yml文件,注意书写格式.如果格式错误,也会导致运行结果报错.")

        return api_data

